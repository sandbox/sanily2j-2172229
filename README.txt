Description
-----------
This module is used to export Subscribers via newsletter module

Requirements
------------
Drupal 7.x
Newsletter Module

Installation
------------
1. Unzip and Copy the module in Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"

3. Navigate to Configuration > Media > Newsletter > Export Subscribers.

4. Select fields you want to export.
